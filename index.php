<?php

/**
* @ Upload essentials files
* @ author: Filipe Cruz
* @ email: fmbcruz@live.com
*/
    /**
     * @ Include routes to use function getUrl();
     */
    include_once 'libs/routes/routes.php';

    /**
     * @ Include respective view
     */
    include_once getView();

