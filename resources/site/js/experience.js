$('.agencia-life').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 03/2016 a 04/2017</p>' +
        '<h3>Agência Life</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Web Developer</p>' +
        '<p>Responsável por fornecer soluções tecnológicas aos clientes da agência, tais como: desenvolvimento de sites, ' +
        'sistemas, e-commerce e apps. Ferramentas utilizadas: Php, MySQL, Front-end (HTML, CSS, Frameworks JS e BootStrap).' +
        '</p>' +
        '<h3>Destaque</h3>' +
        '<p>Desenvolvi uma loja virtual própria para a agência não depender das limitações das plataformas de parceiros. ' +
        'A agência passou a lucrar com hospedagem, antes eles contratavam parceiros para este serviço e tratavam hospedagem como despesa.' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});

$('.geomx').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 05/2017 a 12/2017</p>' +
        '<h3>GeoMX Tecnologia</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Analista de Sistemas Pleno</p>' +
        '<p>Responsável por planejar, coordenar e executar projetos de software de gestão de frotas em tempo real.' +
        'Também responsável por administrar bancos de dados de alta disponibilidade. Habituado com PostgreSQL' +
        '(SQL), PHP e Front-end (JavaScript, JQuery, Ajax, HTML, CSS).' +
        '</p>' +
        '<h3>Destaque</h3>' +
        '<p>Responsável pelo desenvolvimento, planejamento e execução da licitação da CEMIG. O sistema de gestão de' +
        'frotas foi adaptado para atender as necessidades do cliente e entregue antes do prazo estipulado na licitação.' +
        'Além da alta disponibilidade o banco de dados armazena gigabytes e suporta centenas de usuários simultâneos.' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});

$('.tagplus').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 06/2015 a 02/2016</p>' +
        '<h3>Gat Tecnologia</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Analista de Suporte</p>' +
        '<p>Responsável por dar suporte a clientes via chat, telefone e e-mail do software de gestão comercial online.' +
        'Também responsável por criação de landing pages, otimização do SEO, treinamentos e tutoriais.' +
        '</p>' +
        '<h3>Destaque</h3>' +
        '<p>Após o período diário de suporte, executava tarefas extras de desenvolvimento e otimização do blog da empresa.' +
        ' Se tratando de suporte, a nota geral nunca foi menor que 4.5 estrelas além de ter recebido diversos feedbacks positivos.' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});

$('._2ck').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 09/2013 a 08/2014</p>' +
        '<h3>2CK Marketing Digital</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Estagiário em Desenvolvimento Web</p>' +
        '<p>Responsável por planejamento, estruturação e criação de sites responsivos. ' +
        ' Habituado com Wordpress, Magento, Php, Html, Css, JavaScript. Também responsável por campanhas do Google AdWords' +
        ', utilização de ferramentas de diagnóstico e desempenho, divulgação de campanhas em redes sociais.' +
        '</p>' +
        '<h3>Destaque</h3>' +
        '<p>A curva de aprendizado foi muito grande, tanto na área de desenvolvimento quanto na àrea de marketing digital.' +
        ' As campanhas do AdWords chegaram a 25% de Taxa de Cliques (CTR).' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});

$('.tribus').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 09/2013 a 08/2014</p>' +
        '<h3>Tribus Brasil Marketing Digital</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Fundador e Desenvolvedor (Freelancer)</p>' +
        '<p>Desenvolvimento de Sites (Desenvolvedor, Web Designer e Redator), Marketing Digital, ' +
        ' Desenvolvimento de Loja Virtual, Otimização de Sites (SEO) e Links Patrocinados. Responsável por todo' +
        ' processo, desde a captação de cliente, negociação, desenvolvimento e treinamento.' +
        '</p>' +
        '<h3>Destaque</h3>' +
        '<p>O cliente RW Engenharia, passou a ter 15 mil acessos por mês após a reformulação de seu antigo site <a href="http://www.rwengenharia.eng.br" target="_blank">www.rwengenharia.eng.br</a>.' +
        ' O Médico Dr. Rodrigo captou mais clientes com a reformulação de seu antigo site <a href="http://www.medicinaortomolecularbh.com.br" target="_blank">www.medicinaortomolecularbh.com.br</a>.' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});

$('.ductor').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 09/2014 a 05/2015</p>' +
        '<h3>TÜV Rheinland Ductor</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Prestação de Serviços de TI</p>' +
        '<p>Responsável por planejamento, estruturação e manutenção da infraestrutura de TI da empresa.' +
        ' Habituado com Linux, Windows Server, redes, servidores, manutenção de notebooks e desktops.' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});

$('.fundep').click(function(){

    var html = '<p style="margin-bottom: -10px;text-align: center;">De 01/2018 a Atual</p>' +
        '<h3>FUNDEP - Fundação de Desenvolvimento da Pesquisa</h3>' +
        '<p style="text-align: center;margin-top: -10px;">Desenvolvedor de Sistemas Pleno</p>' +
        '<p>Responsável por desenvolver e aplicar melhorias no portal da UFMG e diversos outros sistemas vinculados. ' +
        ' Habituado com MySQL, Zend Framework 2, PHP, Doctrine, Git, API\'s e Front-end (Angular, JavaScript, JQuery, Ajax, HTML, CSS). ' +
        '</p>';

    $('.contact--lockup .modal .modal--information').empty().html(html);

});