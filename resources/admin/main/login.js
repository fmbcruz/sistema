$('document').ready(function(){

    $("#hiddenDiv").removeAttr("hidden");

    $("#btn-login").click(function(){
        var data = $("#login-form").serialize();

        $.ajax({
            type : 'POST',
            url  : '../modules/Admin/Controller/LogController.php',
            data : data,
            dataType: 'json',
            beforeSend: function()
            {
                $("#btn-login").addClass('is-loading');
            },
            success :  function(data){
                if(data.success){
                    $("#btn-login").html('Entrar');
                    $("#login-alert").css('display', 'none');
                    location.href = "/admin";
                }
                else{
                    $("#login-alert").removeAttr("hidden");
                    $("#mensagem").html('').append(data.message);
                }
            }
        });
    });

});