<?php

session_start();

/**
 * require database connection
 */
require '../../../libs/db/database.php';

    /**
     * Constance with the quantity of accepted attempts
     */
    const ACCEPT_ATTEMPTS = 5;

    /**
     * Constance with the quantity of minutes for block
     */
    const BLOCK_MINUTES = 30;

    /**
     * Constance with the login domain
     */
    const LOGIN_DOMAIN = 'http://sistema.dev/admin/login';

    /**
     * @var bool
     */
    $httpRefer = isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !== LOGIN_DOMAIN;

    /**
     * @var string
     */
    $userName = (isset($_POST['userEmail']))    ? $_POST['userEmail']    : '' ;;

    /**
     * @var string
     */
    $userPassword = (isset($_POST['userPassword'])) ? $_POST['userPassword'] : '' ;;


        /**
         *  1 - Verify if the origin of the request it's the same application domain
         */        
        if ($httpRefer):
            $return = array(
                'success' => false,
                'mensagem' => 'Origem da requisição não autorizada!'
            );
            echo json_encode($return);
            exit();
        endif;

        /**
         *  Instance PDO connection
         */
        $connection = Database::getInstance();

        /**
         * 2 - Validation fill form email and password
         */
        if (empty($userName)):
            $return = array(
                'success' => false,
                'message' => 'Preencha seu e-mail!'
            );
            echo json_encode($return);
            exit();
        endif;

        if (empty($userPassword)):
            $return = array(
                'success' => false,
                'message' => 'Preencha sua senha!'
            );
            echo json_encode($return);
            exit();
        endif;

        /**
         * 3 - Validation of the valid email format
         */
        if (!filter_var($userName, FILTER_VALIDATE_EMAIL)):
            $return = array(
                'success' => false,
                'message' => 'Formato de e-mail inválido!'
            );
            echo json_encode($return);
            exit();
        endif;

        /**
         * 4 - Verify if the user already exceeded the quantity of wrong attempts of day
         */
        $sql  = "SELECT count(*) AS attempts, MINUTE(TIMEDIFF(NOW(), MAX(data_hora))) AS minutes ";
        $sql .= "FROM tab_log_tentativa ";
        $sql .= "WHERE ip = ? and DATE_FORMAT(data_hora,'%Y-%m-%d') = ? AND bloqueado = ?";
            $stm = $connection->prepare($sql);
            $stm->bindValue(1, $_SERVER['SERVER_ADDR']);
            $stm->bindValue(2, date('Y-m-d'));
            $stm->bindValue(3, 'SIM');
            $stm->execute();
            $return = $stm->fetch(PDO::FETCH_OBJ);

            if ( !empty($return->attempts) && intval($return->minutes) <= BLOCK_MINUTES ):
                $_SESSION['attempts'] = 0;
                $return = array(
                    'success' => false,
                    'message' => 'Você excedeu o limite de '.ACCEPT_ATTEMPTS.' tentativas, login bloqueado por '.BLOCK_MINUTES.' minutos!');
                echo json_encode($return);
                exit();
            endif;

        /**
         *  5 - Valid of the user data with database
         */
        $sql  = 'SELECT id, nome, senha, email ';
        $sql .= 'FROM tab_usuario ';
        $sql .= 'WHERE email = ? AND status = ? LIMIT 1';
            $stm = $connection->prepare($sql);
            $stm->bindValue(1, $userName);
            $stm->bindValue(2, 'A');
            $stm->execute();
            $return = $stm->fetch(PDO::FETCH_OBJ);
        /**
         *  6 - Valid of the password using the API Password Hash
         */
        if( !empty($return) && password_verify($userPassword, $return->senha) ):
            $_SESSION['id'] = $return->id;
            $_SESSION['nome'] = $return->nome;
            $_SESSION['email'] = $return->email;
            $_SESSION['attempts'] = 0;
            $_SESSION['logado'] = true;
        else:

            $_SESSION['logado'] = false;
            $_SESSION['attempts'] = (isset($_SESSION['attempts'])) ? $_SESSION['attempts'] += 1 : 1;
            $bloqueado = ($_SESSION['attempts'] == ACCEPT_ATTEMPTS) ? 'SIM' : 'NAO';

            /**
             * 7 - Save attempts
             */
            $sql = 'INSERT INTO tab_log_tentativa (ip, email, senha, origem, bloqueado) VALUES (?, ?, ?, ?, ?)';
                $stm = $connection->prepare($sql);
                $stm->bindValue(1, $_SERVER['SERVER_ADDR']);
                $stm->bindValue(2, $userName);
                $stm->bindValue(3, $userPassword);
                $stm->bindValue(4, $_SERVER['HTTP_REFERER']);
                $stm->bindValue(5, $bloqueado);
                $stm->execute();
        endif;

        /**
         * If logged success true, else return error message
         */
        if ($_SESSION['logado'] == true):
            $return = array(
                'success' => true,
                'message' => 'Logado com sucesso!'
            );
            echo json_encode($return);
            exit();
        else:
            if ($_SESSION['attempts'] == ACCEPT_ATTEMPTS):
                $return = array(
                    'success' => false,
                    'message' => 'Você excedeu o limite de '.ACCEPT_ATTEMPTS.' tentativas, login bloqueado por '.BLOCK_MINUTES.' minutos!'
                );
                echo json_encode($return);
                exit();
            else:
                $return = array(
                    'success' => false,
                    'message' => 'Usuário não autorizado, você tem mais '. (ACCEPT_ATTEMPTS - $_SESSION['attempts']) .' tentativa(s) antes do bloqueio!'
                );
                echo json_encode($return);
                exit();
            endif;
        endif;