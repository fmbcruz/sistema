<?php

/**
 * @ Admin Controller
 */

class Index {

    function checkSession($log = false){

        session_start();

        if(!isset($_SESSION['logado'])):
            header("Location: ../admin/login");
        elseif($log):
            header("Location: ../admin");
        endif;
    }

}
