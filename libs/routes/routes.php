<?php

/**
 * @ Prepare routes in project
 * @ author: Filipe Cruz
 * @ email: fmbcruz@live.com
 */

    /**
     * @return string
     */
    function getView(){

        /**
         * @ urlActual = Actual url accessed
         */
        $urlActual = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        /**
         * @ urlParts  = Array with slugs of url
         * @ urlSize   = Number of elements in urlParts
         */
        $urlParts  = explode('/', $urlActual);

        echo '<pre>';
        print_r($urlParts);
        echo '</pre>';

        /**
         * @ Url default of module
         */
        $getView = 'modules/' . ucfirst($urlParts[3]);

        /**
         * @ Case access the root module
         */
        if($urlParts[3] === 'resources'):
            $getView = $urlParts[4];
            foreach ($urlParts as $n => $url):
                if($n > 4):
                    $getView .= '/' . $url;
                endif;
            endforeach;
        elseif($urlParts[3] === ''):
            $getView .= 'Site/Views/index.phtml';
        elseif(isset($urlParts[4]) === ''):
            $getView .= '/Views/index.phtml';
        else:
            $getView .= '/Views';
            /*
             * @ Construction of url View
             */
            foreach ($urlParts as $n => $url):
                if($n > 3):
                    $getView .= '/' . $url;
                endif;
            endforeach;
            $getView .= '/index.phtml';
        endif;

        /**
         * @ Case file not found, will show 404 page
         */
        if(!is_file($getView)):
            $getView ='modules/' . ucfirst($urlParts[3]) . 'Views/error/index.phtml';
        endif;

        return $getView;
    }


